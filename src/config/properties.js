import Cookies from "universal-cookie";
const AUTH_SERVICE = "https://775e7848ea2c.ngrok.io";
const CHAT_SERVICE = "https://bd290e5b7691.ngrok.io";
const properties = {
  INSTA_REDIRECT_URL: process.env.REACT_APP_INSTA_REDIRECT_URL,
  INSTA_ACCESS_TOKEN_URL: "https://api.instagram.com/oauth/access_token",
  INSTA_CLIENT_ID: "156957412902052",
  INSTA_CLIENT_SECRECT: "f8a755b73bb53fdfcdc467ef5aa042d6",
  FB_APP_ID: "358238235599355",

  AUTH_SERVICE: AUTH_SERVICE,
  CHAT_SERVICE: CHAT_SERVICE,
  INSTA_USER_REGISTER: AUTH_SERVICE + "/insta/register/user",
  GET_INSTA_ASSOCIATED_ACCOUNTS: AUTH_SERVICE + "/insta/accounts",
};
export default properties;

export const cookies = new Cookies();
