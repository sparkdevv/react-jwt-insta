import React from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import Signin from "./signin/Signin";
import InstaLogin from "./signin/InstaLogin";
import InstaLoginComponent from "./signin/InstaLoginComponent";
import Signup from "./signup/Signup";
import Chat from "./chat/Chat";
import "./App.css";
import Accounts from "./profile/Accounts";
import Sidebar from "./Sidebar/Sidebar";
import Home from "./components/Home/Home";
import ProtectedRoute from "./components/ProtectedRoute";

export const AppContext = React.createContext();
const App = () => {
  return (
    <main className="my-layout">
      <BrowserRouter>
        <Route path="/" component={Sidebar} />
        <section className="my-content">
          <Switch>
            <Route
              exact
              path="/login"
              render={(props) => <Signin {...props} />}
            />
            <Route
              exact
              path="/signup"
              render={(props) => <Signup {...props} />}
            />
            <Route exact path="/chat" render={(props) => <Chat {...props} />} />
            <Route
              exact
              path="/instalogin"
              render={(props) => <InstaLogin {...props} />}
            />

            <ProtectedRoute path="/home" component={Home} />
            <Redirect from="/" to="/home" />
            {/* <Route exact path="/privacypolicy" render={() => } />
          <Route exact path="/deletedata" render={() => } /> */}
          </Switch>
          <footer className="footer">
            Social Pub ©2018 Created by Vikram Pawar
          </footer>
        </section>
      </BrowserRouter>
    </main>
  );
};

export default App;
